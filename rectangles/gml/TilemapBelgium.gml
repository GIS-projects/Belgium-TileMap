<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation=""
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>2.188199932170027</gml:X><gml:Y>49.86273698575594</gml:Y></gml:coord>
      <gml:coord><gml:X>6.237427654931853</gml:X><gml:Y>51.56784930778024</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                          
  <gml:featureMember>
    <ogr:TilemapBelgium fid="TilemapBelgium.0">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>5.200024059959194,51.564978540717597 5.193569906020552,51.203153919012891 4.278543551384969,51.20600283640487 4.277843651283663,51.567849307780243 5.200024059959194,51.564978540717597</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>1</ogr:id>
      <ogr:nl>Antwerpen</ogr:nl>
      <ogr:fr>Anvers</ogr:fr>
      <ogr:local>Antwerpen</ogr:local>
    </ogr:TilemapBelgium>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:TilemapBelgium fid="TilemapBelgium.1">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>5.324152314518858,51.123558000592006 6.237427654931853,51.11258313433386 6.223015911827483,50.750447971453369 5.316761123443218,50.761339609634007 5.324152314518858,51.123558000592006</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>5</ogr:id>
      <ogr:nl>Limburg</ogr:nl>
      <ogr:fr>Limbourg</ogr:fr>
      <ogr:local>Limburg</ogr:local>
    </ogr:TilemapBelgium>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:TilemapBelgium fid="TilemapBelgium.2">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>5.192180545163093,51.124516549561193 5.185807043679473,50.762446654468498 4.279344528034457,50.765306060462187 4.278694150187661,51.127397826667654 5.192180545163093,51.124516549561193</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>4</ogr:id>
      <ogr:nl>Vlaams-Brabant</ogr:nl>
      <ogr:fr>Brabant flamand</ogr:fr>
      <ogr:local>Vlaams-Brabant</ogr:local>
    </ogr:TilemapBelgium>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:TilemapBelgium fid="TilemapBelgium.3">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>4.146704806913141,51.127186967184279 4.148370132286707,50.765096467783124 3.241861872509675,50.759922569029214 3.233173116857099,51.121973593097032 4.146704806913141,51.127186967184279</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>3</ogr:id>
      <ogr:nl>Oost-Vlaanderen</ogr:nl>
      <ogr:fr>Flandre-Orientale</ogr:fr>
      <ogr:local>Oost-Vlaanderen</ogr:local>
    </ogr:TilemapBelgium>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:TilemapBelgium fid="TilemapBelgium.4">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3.101218531820655,51.120593323682662 3.110921439710981,50.758552427746956 2.204916028725767,50.745349348599191 2.188199932170027,51.107289485402241 3.101218531820655,51.120593323682662</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>2</ogr:id>
      <ogr:nl>West-Vlaanderen</ogr:nl>
      <ogr:fr>Flandre-Occidentale</ogr:fr>
      <ogr:local>West-Vlaanderen</ogr:local>
    </ogr:TilemapBelgium>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:TilemapBelgium fid="TilemapBelgium.5">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>4.148772876124873,50.686518221718153 4.150442681484739,50.324574764964396 3.252334955008376,50.319411896789887 3.243775335853192,50.681316103686818 4.148772876124873,50.686518221718153</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>6</ogr:id>
      <ogr:nl>Henegouwen</ogr:nl>
      <ogr:fr>Hainaut</ogr:fr>
      <ogr:local>Hainaut</ogr:local>
    </ogr:TilemapBelgium>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:TilemapBelgium fid="TilemapBelgium.6">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>5.184481662144996,50.683872455245186 5.178265576980722,50.321949017905219 4.280203332822388,50.324745655529377 4.279529161210319,50.686690407714231 5.184481662144996,50.683872455245186</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>7</ogr:id>
      <ogr:nl>Waals-Brabant</ogr:nl>
      <ogr:fr>Brabant wallon</ogr:fr>
      <ogr:local>Brabant wallon</ogr:local>
    </ogr:TilemapBelgium>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:TilemapBelgium fid="TilemapBelgium.7">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>6.219969975774983,50.672049011030737 6.205874461632663,50.31021488321003 5.308008741582971,50.320970026090023 5.315220054823429,50.682886009422027 6.219969975774983,50.672049011030737</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>8</ogr:id>
      <ogr:nl>Luik</ogr:nl>
      <ogr:fr>Li?ge</ogr:fr>
      <ogr:local>Li?ge</ogr:local>
    </ogr:TilemapBelgium>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:TilemapBelgium fid="TilemapBelgium.8">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>6.202842001409847,50.231641775145064 6.188761990035539,49.862736985755944 5.299265547771119,49.873367496593538 5.306456846078792,50.242354529810399 6.202842001409847,50.231641775145064</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>10</ogr:id>
      <ogr:nl>Luxemburg</ogr:nl>
      <ogr:fr>Luxembourg</ogr:fr>
      <ogr:local>Luxembourg</ogr:local>
    </ogr:TilemapBelgium>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:TilemapBelgium fid="TilemapBelgium.9">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>5.176928278542264,50.243356634845505 5.170848316042649,49.881292010622893 4.281034582826933,49.884038446409072 4.280348421150229,50.246123948368336 5.176928278542264,50.243356634845505</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>9</ogr:id>
      <ogr:nl>Namen</ogr:nl>
      <ogr:fr>Namur</ogr:fr>
      <ogr:local>Namur</ogr:local>
    </ogr:TilemapBelgium>
  </gml:featureMember>
</ogr:FeatureCollection>
