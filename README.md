# Tile Map of Belgium
This repository contains Tile Maps of Belgian provinces in different formats.

The following formats are available:
* a shape file (EPSG:31370)
* a geojson file (EPSG:4326)
* a gml file (EPSG:4326)
* a svg file
* a pdf file (A4 format & minimal format)
* an eps file
* png files (in 3 sizes)
* jpg files (in 3 sizes)
* cad files (dwg & dxf format)
* excel files (xlsx & xls format, only the rectangular tilemap)
* odf spreadsheet (LibreOffice, only the rectangular tilemap)
* a csv file (without formatting, only the rectangular tilemap)

## Rectangles
![Example rectangle png file](https://gitlab.com/GIS-projects/Belgium-TileMap/raw/master/rectangles/png/TilemapBelgiumSmall.png "Example png file")

## Hexagons
### Version 1
![Example hexagon version 1 png file](https://gitlab.com/GIS-projects/Belgium-TileMap/raw/master/hexagons/png/TilemapBelgiumHexagonV1Small.png "Example png file")

### Version 2
![Example hexagon version 1 png file](https://gitlab.com/GIS-projects/Belgium-TileMap/raw/master/hexagons/png/TilemapBelgiumHexagonV2Small.png "Example png file")