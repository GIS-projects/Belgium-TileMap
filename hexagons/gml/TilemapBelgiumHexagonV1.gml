<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation=""
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>2.812870281488472</gml:X><gml:Y>49.83941924584226</gml:Y></gml:coord>
      <gml:coord><gml:X>6.368156705087715</gml:X><gml:Y>51.55272429225269</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                          
  <gml:featureMember>
    <ogr:TilemapBelgiumHexagonV1 fid="TilemapBelgiumHexagonV1.0">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>5.07015450995233,49.8425745343426 4.87185317806675,50.0683872364202 5.07678918723612,50.2920845090465 5.48186565235325,50.2888994455717 5.67819112531673,50.0620470464299 5.47143975514076,49.8394192458423 5.07015450995233,49.8425745343426</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>9</ogr:id>
      <ogr:nl>Namen</ogr:nl>
      <ogr:fr>Namur</ogr:fr>
      <ogr:local>Namur</ogr:local>
    </ogr:TilemapBelgiumHexagonV1>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:TilemapBelgiumHexagonV1 fid="TilemapBelgiumHexagonV1.1">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>4.39371652948997,50.0964538337954 4.19115393387072,50.3210853086087 4.39396019861994,50.5459749637029 4.80124931270677,50.5451726844753 5.00187491325559,50.3194883188552 4.7971725575087,50.0956590820448 4.39371652948997,50.0964538337954</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>7</ogr:id>
      <ogr:nl>Waals-Brabant</ogr:nl>
      <ogr:fr>Brabant wallon</ogr:fr>
      <ogr:local>Brabant wallon</ogr:local>
    </ogr:TilemapBelgiumHexagonV1>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:TilemapBelgiumHexagonV1 fid="TilemapBelgiumHexagonV1.2">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3.71246397517954,50.3463394780358 3.50560627944154,50.5697582320331 3.70620220143706,50.7958254583609 4.11565829192146,50.7974231297602 4.3206202324341,50.5729385340615 4.1180463842973,50.3479221909709 3.71246397517954,50.3463394780358</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>6</ogr:id>
      <ogr:nl>Henegouwen</ogr:nl>
      <ogr:fr>Hainaut</ogr:fr>
      <ogr:local>Hainaut</ogr:local>
    </ogr:TilemapBelgiumHexagonV1>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:TilemapBelgiumHexagonV1 fid="TilemapBelgiumHexagonV1.3">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3.02406647325599,50.5921757393833 2.81287028148847,50.8143460138683 3.01116286804226,51.0415795721755 3.42273758108533,51.0456022452575 3.63208189122967,50.8223533983777 3.43172828281936,50.5961606597724 3.02406647325599,50.5921757393833</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>2</ogr:id>
      <ogr:nl>West-Vlaanderen</ogr:nl>
      <ogr:fr>Flandre-Occidentale</ogr:fr>
      <ogr:local>West-Vlaanderen</ogr:local>
    </ogr:TilemapBelgiumHexagonV1>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:TilemapBelgiumHexagonV1 fid="TilemapBelgiumHexagonV1.4">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3.70544274495016,50.8497607400203 3.49632569939721,51.0731384843519 3.6990455388574,51.2991851744632 4.11292894556151,51.3007998243594 4.32010966878043,51.0763525288167 4.1153686551676,50.8513602190695 3.70544274495016,50.8497607400203</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>3</ogr:id>
      <ogr:nl>Oost-Vlaanderen</ogr:nl>
      <ogr:fr>Flandre-Orientale</ogr:fr>
      <ogr:local>Oost-Vlaanderen</ogr:local>
    </ogr:TilemapBelgiumHexagonV1>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:TilemapBelgiumHexagonV1 fid="TilemapBelgiumHexagonV1.5">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>5.08280989211933,50.8494546653771 4.88030182659497,51.0752491864579 5.08971034417397,51.2988761275812 5.50354499108329,51.2956314086781 5.70398967986517,51.0687904611229 5.49268842831694,50.8462404887918 5.08280989211933,50.8494546653771</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>5</ogr:id>
      <ogr:nl>Limburg</ogr:nl>
      <ogr:fr>Limbourg</ogr:fr>
      <ogr:local>Limburg</ogr:local>
    </ogr:TilemapBelgiumHexagonV1>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:TilemapBelgiumHexagonV1 fid="TilemapBelgiumHexagonV1.6">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>4.39398975350062,50.599915513061 4.18926026730922,50.8245274533205 4.39423872643649,51.0493926480069 4.80590829445957,51.0485818253477 5.00865887272095,50.8229134841191 4.80174373789349,50.5991123242117 4.39398975350062,50.599915513061</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>4</ogr:id>
      <ogr:nl>Vlaams-Brabant</ogr:nl>
      <ogr:fr>Brabant flamand</ogr:fr>
      <ogr:local>Vlaams-Brabant</ogr:local>
    </ogr:TilemapBelgiumHexagonV1>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:TilemapBelgiumHexagonV1 fid="TilemapBelgiumHexagonV1.7">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>4.39426892847691,51.1033258429139 4.18732568976578,51.3279009214271 4.39452337883448,51.5527242922527 4.81066864968957,51.5519048039781 5.01558969246972,51.3262697313718 4.80641342586762,51.102514097699 4.39426892847691,51.1033258429139</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>1</ogr:id>
      <ogr:nl>Antwerpen</ogr:nl>
      <ogr:fr>Anvers</ogr:fr>
      <ogr:local>Antwerpen</ogr:local>
    </ogr:TilemapBelgiumHexagonV1>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:TilemapBelgiumHexagonV1 fid="TilemapBelgiumHexagonV1.8">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>5.07641526124749,50.3460294975202 4.87603264062551,50.571841665063 5.08318076469106,50.7955124794485 5.49258948133163,50.7922977833703 5.69095319506996,50.5654425924217 5.48195162877246,50.34284495497 5.07641526124749,50.3460294975202</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>8</ogr:id>
      <ogr:nl>Luik</ogr:nl>
      <ogr:fr>Liège</ogr:fr>
      <ogr:local>Liège</ogr:local>
    </ogr:TilemapBelgiumHexagonV1>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:TilemapBelgiumHexagonV1 fid="TilemapBelgiumHexagonV1.9">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>5.75387439689673,50.0881390445278 5.55780213108899,50.3151195866902 5.767037629533,50.5375815427504 6.17414090985974,50.5319808365226 6.36815670508772,50.3039708805307 6.157149787845,50.0825907578899 5.75387439689673,50.0881390445278</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>10</ogr:id>
      <ogr:nl>Luxemburg</ogr:nl>
      <ogr:fr>Luxembourg</ogr:fr>
      <ogr:local>Luxembourg</ogr:local>
    </ogr:TilemapBelgiumHexagonV1>
  </gml:featureMember>
</ogr:FeatureCollection>
