<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation=""
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>2.812870281488472</gml:X><gml:Y>49.83941680740514</gml:Y></gml:coord>
      <gml:coord><gml:X>5.70398967986517</gml:X><gml:Y>51.55272429225269</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                           
  <gml:featureMember>
    <ogr:TilemapBelgiumHexagonV2 fid="TilemapBelgiumHexagonV2.0">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>4.39371652948997,50.0964538337954 4.19115393387072,50.3210853086087 4.39396019861994,50.5459749637029 4.80124931270677,50.5451726844753 5.00187491325559,50.3194883188552 4.7971725575087,50.0956590820448 4.39371652948997,50.0964538337954</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>7</ogr:id>
      <ogr:nl>Namen</ogr:nl>
      <ogr:fr>Namur</ogr:fr>
      <ogr:local>Namur</ogr:local>
    </ogr:TilemapBelgiumHexagonV2>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:TilemapBelgiumHexagonV2 fid="TilemapBelgiumHexagonV2.1">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3.71246397517954,50.3463394780358 3.50560627944154,50.5697582320331 3.70620220143706,50.7958254583609 4.11565829192146,50.7974231297602 4.3206202324341,50.5729385340615 4.1180463842973,50.3479221909709 3.71246397517954,50.3463394780358</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>6</ogr:id>
      <ogr:nl>Henegouwen</ogr:nl>
      <ogr:fr>Hainaut</ogr:fr>
      <ogr:local>Hainaut</ogr:local>
    </ogr:TilemapBelgiumHexagonV2>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:TilemapBelgiumHexagonV2 fid="TilemapBelgiumHexagonV2.2">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3.02406647325599,50.5921757393833 2.81287028148847,50.8143460138683 3.01116286804226,51.0415795721755 3.42273758108533,51.0456022452575 3.63208189122967,50.8223533983777 3.43172828281936,50.5961606597724 3.02406647325599,50.5921757393833</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>2</ogr:id>
      <ogr:nl>West-Vlaanderen</ogr:nl>
      <ogr:fr>Flandre-Occidentale</ogr:fr>
      <ogr:local>West-Vlaanderen</ogr:local>
    </ogr:TilemapBelgiumHexagonV2>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:TilemapBelgiumHexagonV2 fid="TilemapBelgiumHexagonV2.3">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3.70544274495016,50.8497607400203 3.49632569939721,51.0731384843519 3.6990455388574,51.2991851744632 4.11292894556151,51.3007998243594 4.32010966878043,51.0763525288167 4.1153686551676,50.8513602190695 3.70544274495016,50.8497607400203</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>3</ogr:id>
      <ogr:nl>Oost-Vlaanderen</ogr:nl>
      <ogr:fr>Flandre-Orientale</ogr:fr>
      <ogr:local>Oost-Vlaanderen</ogr:local>
    </ogr:TilemapBelgiumHexagonV2>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:TilemapBelgiumHexagonV2 fid="TilemapBelgiumHexagonV2.4">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>5.08280989211933,50.8494546653771 4.88030182659497,51.0752491864579 5.08971034417397,51.2988761275812 5.50354499108329,51.2956314086781 5.70398967986517,51.0687904611229 5.49268842831694,50.8462404887918 5.08280989211933,50.8494546653771</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>5</ogr:id>
      <ogr:nl>Limburg</ogr:nl>
      <ogr:fr>Limbourg</ogr:fr>
      <ogr:local>Limburg</ogr:local>
    </ogr:TilemapBelgiumHexagonV2>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:TilemapBelgiumHexagonV2 fid="TilemapBelgiumHexagonV2.5">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>4.39398975350062,50.599915513061 4.21446212670663,50.7970133552545 4.98318834999533,50.7954991856715 4.80174373789349,50.5991123242117 4.39398975350062,50.599915513061</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>4</ogr:id>
      <ogr:nl>Waals-Brabant</ogr:nl>
      <ogr:fr>Brabant wallon</ogr:fr>
      <ogr:local>Brabant wallon</ogr:local>
    </ogr:TilemapBelgiumHexagonV2>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:TilemapBelgiumHexagonV2 fid="TilemapBelgiumHexagonV2.6">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>4.21433330165527,50.8521690143034 4.39423872643649,51.0493926480069 4.80590829445957,51.0485818253477 4.98385872196964,50.8506532886612 4.21433330165527,50.8521690143034</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>4</ogr:id>
      <ogr:nl>Vlaams-Brabant</ogr:nl>
      <ogr:fr>Brabant flamand</ogr:fr>
      <ogr:local>Vlaams-Brabant</ogr:local>
    </ogr:TilemapBelgiumHexagonV2>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:TilemapBelgiumHexagonV2 fid="TilemapBelgiumHexagonV2.7">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>4.39426892847691,51.1033258429139 4.18732568976578,51.3279009214271 4.39452337883448,51.5527242922527 4.81066864968957,51.5519048039781 5.01558969246972,51.3262697313718 4.80641342586762,51.102514097699 4.39426892847691,51.1033258429139</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>1</ogr:id>
      <ogr:nl>Antwerpen</ogr:nl>
      <ogr:fr>Anvers</ogr:fr>
      <ogr:local>Antwerpen</ogr:local>
    </ogr:TilemapBelgiumHexagonV2>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:TilemapBelgiumHexagonV2 fid="TilemapBelgiumHexagonV2.8">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>5.07641526124749,50.3460294975202 4.87603264062551,50.571841665063 5.08318076469106,50.7955124794485 5.49258948133163,50.7922977833703 5.69095319506996,50.5654425924217 5.48195162877246,50.34284495497 5.07641526124749,50.3460294975202</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>8</ogr:id>
      <ogr:nl>Luik</ogr:nl>
      <ogr:fr>Liège</ogr:fr>
      <ogr:local>Liège</ogr:local>
    </ogr:TilemapBelgiumHexagonV2>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:TilemapBelgiumHexagonV2 fid="TilemapBelgiumHexagonV2.9">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>5.07015478839522,49.8425720968422 4.87185346793781,50.068384799414 5.07678946796997,50.292082071576 5.48186591225519,50.2888970071561 5.67819137396887,50.0620446075415 5.47144001313971,49.8394168074051 5.07015478839522,49.8425720968422</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>10</ogr:id>
      <ogr:nl>Luxemburg</ogr:nl>
      <ogr:fr>Luxembourg</ogr:fr>
      <ogr:local>Luxembourg</ogr:local>
    </ogr:TilemapBelgiumHexagonV2>
  </gml:featureMember>
</ogr:FeatureCollection>
